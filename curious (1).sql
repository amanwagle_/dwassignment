-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 28, 2019 at 05:39 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.3.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `curious`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_bookings`
--

CREATE TABLE `tbl_bookings` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `training_id` int(11) NOT NULL,
  `booked_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_bookings`
--

INSERT INTO `tbl_bookings` (`id`, `user_id`, `training_id`, `booked_date`) VALUES
(1, 2, 3, '2019-04-26 09:04:02'),
(2, 2, 2, '2019-04-26 09:04:13');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_contacts`
--

CREATE TABLE `tbl_contacts` (
  `id` int(11) NOT NULL,
  `name` varchar(20) NOT NULL,
  `email` varchar(50) NOT NULL,
  `phone_num` varchar(20) NOT NULL,
  `subject` varchar(20) NOT NULL,
  `message` varchar(1000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_contacts`
--

INSERT INTO `tbl_contacts` (`id`, `name`, `email`, `phone_num`, `subject`, `message`) VALUES
(1, 'Aman Wagle', 'amanwagle6767@gmail.com', '9846768689', 'Cyber Security', 'Which training will be sutiable to learn about network security'),
(2, 'Jeewan Dhakal', 'jeevandhakal25@gmail.com', '52112124154', 'Web Security', 'What is the function of https?'),
(3, 'Prajwal Pradhan', 'pradhanprajwal5@gmail.com', '2211515123', 'Encryption', 'How can we encrypt our file?');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_trainings`
--

CREATE TABLE `tbl_trainings` (
  `id` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `date` text NOT NULL,
  `start_time` text NOT NULL,
  `end_time` text NOT NULL,
  `cost` float NOT NULL,
  `area_covered` varchar(200) NOT NULL,
  `image` varchar(200) NOT NULL,
  `des` varchar(1000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_trainings`
--

INSERT INTO `tbl_trainings` (`id`, `title`, `date`, `start_time`, `end_time`, `cost`, `area_covered`, `image`, `des`) VALUES
(1, 'SANS Cyber Security Classes', '2019-05-31', '13:00', '16:00', 200, 'Ethical Hacking', '1556261305_11984.png', 'Advance your career and explore our cyber security classes and cyber security skills roadmap, including our site focused on SANS cyber security training in the UK and Europe. Our curricula span from the core essential technical skills and strategies that every cyber security professional should know, to advanced topics in penetration testing and ethical hacking, digital forensics and incident response, defense of critical infrastructure, application development, and audit, and legal topics.'),
(2, 'Cyber Security certifications', '2019-04-30', '08:00', '12:00', 500, 'Software security', '1556261395_70310.png', 'GIAC (Global Information Assurance Certification) was founded in 1999 to validate the skills of cyber security professionals. The purpose of GIAC is to provide assurance that a certified individual has the knowledge and skills necessary for a practitioner in key areas of computer, information and software security. GIAC certifications are trusted by thousands of companies and government agencies, and are unique because they measure specific skills and knowledge areas rather than general infosec knowledge.'),
(3, 'Firewall Management', '2019-06-22', '08:00', '13:00', 800, 'Firewall, Network Security', '1556262222_37392.jpg', 'Our curricula span from the core essential technical skills and strategies that every cyber security professional should know, to advanced topics in penetration testing and ethical hacking, digital forensics and incident response, defense of critical infrastructure, application development, and audit, and legal topics.');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `id` int(11) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `surname` varchar(50) NOT NULL,
  `business` varchar(100) NOT NULL,
  `job` varchar(50) NOT NULL,
  `interest` varchar(150) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(20) NOT NULL,
  `customer_number` varchar(15) NOT NULL,
  `role` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`id`, `firstname`, `surname`, `business`, `job`, `interest`, `email`, `password`, `customer_number`, `role`) VALUES
(1, '', '', '', '', '', 'admin@gmail.com', 'admin', '100000_CUR', 1),
(2, 'Aman', 'Wagle', 'Cisco', 'Ethical Hacker', 'Hacking, Security', 'amanwagle689@gmail.com', 'aman123', '758490_CUR', 0),
(3, 'Prajwal', 'Pradhan', 'Aruba', 'Network Specialist', 'Network Security, Firewall', 'pradhanprajwal5@gmail.com', 'prajwal123', '859473_CUR', 0),
(6, 'Jeewan', 'Dhakal', 'Juniper', 'Network Administrator.', 'Programming, Web Developing', 'jeewandhakal25@gmail.com', 'jeewan123', '927356_CUR', 0),
(7, 'White', 'Devil', 'Huewai', 'Network Analyst.', 'Cyber Security, DMZ', 'whitedevil10@gmail.com', 'white123', '849375_CUR', 0),
(9, '', '', '', '', '', 'amanwagle6767@gmail.com', 'hellp', '112679_CUR', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_bookings`
--
ALTER TABLE `tbl_bookings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_contacts`
--
ALTER TABLE `tbl_contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_trainings`
--
ALTER TABLE `tbl_trainings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_bookings`
--
ALTER TABLE `tbl_bookings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_contacts`
--
ALTER TABLE `tbl_contacts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbl_trainings`
--
ALTER TABLE `tbl_trainings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
