<?php
	session_start();
	if(!$_SESSION['auth_admin'])
	{
		header('location:login.php');
	}
include('admin-navbar.php');
require('../connection.php');
?>
<div class="container-fluid">
	<?php include('../messages.php') ?>
	<div class="card mt-2">
		<div class="card-header">
			<span class="table-heading">Customers</span>
			<span class="float-right"><a href="addtraining.php" class="btn btn-sm btn-success"><i class="fa fa-plus"></i>&nbsp; Add Training</a></span>
		</div>
		<div class="card-body">
			<div class="table-responsive">	
				<table class="table">
						<thead>
							<tr>
								<th>S.N.</th>
								<th>Title</th>
								<th>Image</th>
								<th>Date</th>
								<th>Time</th>
								<th>Cost</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							<?php
								$q = "SELECT * from tbl_trainings";
								$data = $con->query($q);
								$n =1; //for serial number
								while($eachTraining = $data->fetch_array())
								{
							?>
								<tr>
									<th scope="row"><?php echo $n; ?></th>
									<td><?php echo $eachTraining['title']; ?></td>
									<td><img src="../assets/images/<?php echo $eachTraining['image'];?>" style="width:90px;"></td>
									<td><?php echo $eachTraining['date']; ?></td>
									<td><?php echo $eachTraining['start_time']." to ".$eachTraining['end_time'];
									?></td>
									<td>$ <?php echo $eachTraining['cost']; ?></td>
									<td class="text-right">
										<a type="button" class="btn btn-primary btn-sm" href="edit-trainings.php?id=<?php echo $eachTraining['id']; ?>"><i class="fa fa-edit" style="font-size: 15px;" ></i></a>
										<a type="button" class="btn btn-danger btn-sm" href="dbwork.php?deleteTraining=<?php echo $eachTraining['id']; ?>"><i class="fa fa-trash"  style="font-size: 15px;"
										onClick="return confirm('Are you sure you want to delete training?')"
											></i></a>
									</td>
								</tr>
							<?php
							$n = $n+1;
							}
							?>
						</tbody>
				</table>
			</div>
		</div>
	</div>
	
</div>
<?php
include('admin-footer.php');
?>