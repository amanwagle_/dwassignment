<?php
	session_start();
	if(!$_SESSION['auth_admin'])
	{
		header('location:login.php');
	}
include('admin-navbar.php');
require('../connection.php');
?>
<div class="container-fluid">
	<?php include('../messages.php'); ?>
	<div class="card mt-2">
		<div class="card-header">
			<span class="table-heading">Customers</span>
		</div>
		<div class="card-body">
			<div class="table-responsive">
				<table class="table">
					<thead>
						<tr>
							<th>S.N.</th>
							<th>Name</th>
							<th>Email</th>
							<th>Business Name</th>
							<th>Job Title</th>
							<th>Interests</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						<?php
							$q = "SELECT * from tbl_users where `role`= 0";
							$data = $con->query($q);
							$n =1; //for serial number
							while($eachCustomer = $data->fetch_array())
							{
						?>
						<tr>
							<th scope="row"><?php echo $n; ?></th>
							<td><?php echo $eachCustomer['firstname']."".$eachCustomer['surname']; ?></td>
							<td><?php echo $eachCustomer['email']; ?></td>
							<td><?php echo $eachCustomer['business']; ?></td>
							<td><?php echo $eachCustomer['job']; ?></td>
							<td><?php echo $eachCustomer['interest']; ?></td>
							<td class="text-right">
								<a type="button" class="btn btn-success btn-sm" href="customers-details.php?customerID=<?php echo $eachCustomer['id']; ?>"><i class="fa fa-search-plus" style="font-size: 15px;" ></i></a>
								<a type="button" class="btn btn-danger btn-sm" href="dbwork.php?deleteCustomer=<?php echo $eachCustomer['id']; ?>" onClick="return confirm('Are you sure you want to delete Customer?')"><i class="fa fa-trash" style="font-size: 15px;"></i></a>
							</td>
						</tr>
						<?php
						$n = $n+1;
						}
						?>
					</tbody>
				</table>
			</div>
		</div>
	</div>	
</div>
<?php
include('admin-footer.php');
?>