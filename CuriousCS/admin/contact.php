<?php
	session_start();
	if(!$_SESSION['auth_admin'])
	{
		header('location:login.php');
	}
include('admin-navbar.php');
require('../connection.php');
?>
<div class="container-fluid">
	<?php
		include('../messages.php');
		$q = "SELECT * from tbl_contacts where `id`=".$_GET['contactID'];
		$contact_detail = $con->query($q)->fetch_array();
	?>
	<div class="card mt-2">
		<div class="card-header">
			<h5>Contact Details</h5>
		</div>
		<div class="card-body">
			<div class="container ml-5">
				<p><b>Name:</b> <?php echo $contact_detail['name']?></p>
				<p><b>Email:</b> <?php echo $contact_detail['email'];?></p>
				<p><b>Phone number:</b> <?php echo $contact_detail['phone_num'];?></p>
				<p><b>Subject:</b> <?php echo $contact_detail['subject'];?></p>
				<p><b>Message:</b><br> <?php echo $contact_detail['message'];?></p>
			</div>
		</div>
		<div class="card-footer">
			<a type="button" class="btn btn-danger btn-sm" href="dbwork.php?deleteContact=<?php echo $contact_detail['id'];?>" onClick="return confirm('Are you sure you want to delete contact details?')"><i class="fa fa-trash" style="font-size: 15px;">&nbsp; Delete</i></a>
		</div>
	</div>
</div>
<?php
include('admin-footer.php');
?>