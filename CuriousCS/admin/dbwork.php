<?php 
	session_start();
	require('../connection.php');
	
	//checks admin login
	if(isset($_POST['login']))
	{
		$email = $_POST['email'];
		$pw = $_POST['password'];

		$q = "SELECT * from tbl_users where (email='$email' and password='$pw') and role=1";
		$admin = $con-> query($q) -> fetch_array();

		if($admin)
		{
			$_SESSION['auth_admin'] = $admin;
			header('location:index.php');
		}
		else
		{
			$_SESSION['error']='Invalid Email or Password!!!';
			header('location:login.php');
		}
	}

	//add trainings
	if(isset($_POST['save-training']))
	{
		$title = $_POST['title'];
		$cost = $_POST['cost'];
		$cs = $_POST['cs'];
		$date = $_POST['date'];
		$stime = $_POST['stime'];
		$etime = $_POST['etime'];
		$des = $_POST['des'];

		//img uploads
	    $imgName = $_FILES['img']['name']; 

		//file extension
		$type = strtolower(pathinfo($imgName, PATHINFO_EXTENSION));
	 
		//unique name of a picture
 		$img_name = time().'_'. rand(1000, 100000).'.'.$type;

		if ($type == "jpg" || $type == "png" || $type == "gif" || $type == "jpeg") 
		{
			move_uploaded_file($_FILES['img']['tmp_name'], '../assets/images/'.$img_name);
			$q= "INSERT INTO `tbl_trainings`(`id`,`title`,`date`,`start_time`,`end_time`,`cost`,`area_covered`,`image`,`des`) VALUES (null,'$title','$date','$stime','$etime','$cost','$cs', '$img_name','$des')";
 
			$con->query($q);
			$_SESSION['success']='Training added successfully.';

			header('location:trainings.php');
		}
		else
		{
			$_SESSION['error']='The image must be jpg, png or gif format.';
			header('location:addtraining.php');
		}
	}

	//delete tranings
	if(isset($_GET['deleteTraining']))
	{
		$trainingID = $_GET['deleteTraining'];
		$q = "DELETE FROM `tbl_trainings` WHERE `id`=". $trainingID;
		$con->query($q);

		$_SESSION['success']='Training deleted successfully.';
		header('location:trainings.php'); 
	}

	//delete customer
	if(isset($_GET['deleteCustomer']))
	{
		$customerID = $_GET['deleteCustomer'];
		$q = "DELETE FROM `tbl_users` WHERE `id`=". $customerID;
		$con->query($q);

		$_SESSION['success']='Customer deleted successfully.';
		header('location:customers.php'); 
	}

	//delete contact details
	if(isset($_GET['deleteContact']))
	{
		$contactID = $_GET['deleteContact'];
		$q = "DELETE FROM `tbl_contacts` WHERE `id`=". $contactID;
		$con->query($q);
		$_SESSION['success']='Contact details deleted successfully.';

		header('location:contact-details.php'); 
	}

	//update training details
	if(isset($_POST['update-training']))
	{
		$id = $_POST['id'];
		$title = $_POST['title'];
		$cost = $_POST['cost'];
		$cs = $_POST['cs'];
		$date = $_POST['date'];
		$stime = $_POST['stime'];
		$etime = $_POST['etime'];
		$des = $_POST['des'];

		$update = "UPDATE `tbl_trainings` SET `title`='$title',`date`='$date',`start_time`='$stime',`end_time`='$etime',`cost`='$cost',`area_covered`='$cs',`des`='$des' where id = $id";

		$con->query($update);
		$_SESSION['success']='Training Details updated successfully.';


		header('location:trainings.php');

	}

	//delete or cancel booking
	if(isset($_GET['deleteBooking']))
	{
		$bookingID = $_GET['deleteBooking'];
		$q = "DELETE FROM `tbl_bookings` WHERE `id`=". $bookingID;
		$con->query($q);

		$_SESSION['success']='Booking cancelled successfully.';
		header('location:bookings.php'); 
	}

 ?>