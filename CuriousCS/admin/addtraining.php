<?php
	session_start();
	if(!$_SESSION['auth_admin'])
	{
		header('location:login.php');
	}
include('admin-navbar.php');
require('../connection.php');
?>
<div class="container-fluid">
	<?php include"../messages.php" ?>
	<div class="my-4">
		<div class="card mt-2">
			<div class="card-header">
				<span class="table-heading">Add Trainings</span>
			</div>
			<div class="card-body">
				<form method="post" action="dbwork.php" enctype="multipart/form-data">
					<div class="form-group row">
						<div class="col-sm-6">
							<label for="txttitle"><b>Title</b></label>
							<input type="text" class="form-control" id="txttitle" placeholder="Title" name="title" required>
						</div>
						<div class="col-sm-6">
							<label for="customFile"><b>Choose Picture</b></label>
							<input type="file" class="form-control" name="img">
						</div>
					</div>
					<div class="form-group">
						<label for="txtcs"><b>Area of Cyber Security</b></label>
						<input type="text" class="form-control" id="txtcs" placeholder="Area of Cyber Security" name="cs" required>
					</div>
					<div class="form-group row">
						<div class="col-sm-6">
							<label for="txtcost"><b>Cost</b></label>
							<input type="number" class="form-control" id="txtcost" placeholder="Cost" name="cost" required>
						</div>
						<div class="col-sm-6">
							<label for="date"><b>Date</b></label>
							<input type="date" class="form-control" id="date" name="date" required>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-sm-6">
							<label for="start-time"><b>Starts In</b></label>
							<input type="time" class="form-control" id="start-time" name="stime" required>
						</div>
						<div class="col-sm-6">
							<label for="end-time"><b>Ends In</b></label>
							<input type="time" class="form-control" id="end-time" name="etime" required>
						</div>
					</div>
					<div class="form-group">
						<label for="txtdes"><b>Description</b></label>
						<textarea class="form-control" id="txtdes" rows="4" placeholder="Description" name="des"></textarea>
					</div>
					<button class="btn btn-success" type="submit" name="save-training">Save</button>
					<button class="btn btn-danger" type="reset">Cancel</button>
				</form>
			</div>
		</div>
	</div>
</div>

<?php
include('admin-footer.php');
?>