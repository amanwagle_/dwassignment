<?php
	session_start();
	if(!$_SESSION['auth_admin'])
	{
		header('location:login.php');
	} 
  include('admin-navbar.php');
 ?>
 <div class="container-fluid">
	<?php include('../messages.php'); ?>
    <h1>Hello Admin.</h1>
 </div>

<?php 
  include('admin-footer.php');
 ?>