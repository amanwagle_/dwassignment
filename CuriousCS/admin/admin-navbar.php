<!DOCTYPE html>
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Curious-CS Admin Panel</title>
    <link rel="stylesheet" type="text/css" href="../assets/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="../assets/css/font-awesome.css">
    <link rel="stylesheet" type="text/css" href="../assets/css/admin.css">
  </head>
  <body class="admin-main-body">

    <!-- MOBILE NAVBAR -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark d-md-none">
      <a class="navbar-brand" href="#">Curious</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav">
          <li class="nav-item active">
            <a class="nav-link" href="trainings.php">Tranings</a>
          </li>
          <li class="nav-item">
            <a href="customers.php" class="nav-link">Customers</a>
          </li>
          <li class="nav-item">
            <a href="bookings.php" class="nav-link">Bookings</a>
          </li>
          <li class="nav-item">
            <a href="contact-details.php" class="nav-link">Contact Information</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="dbinteract.php?logout=true"><i class="fa fa-sign-out"></i>&nbsp;Log Out</a>
          </li>
        </ul>
      </div>
    </nav>
    <!-- MOBILE NAVBAR END -->
    <!-- LARGE SCREEN TOP NAV -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav mr-auto">
        <a class="navbar-brand" href="#">Curious</a>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-user"></i>&nbsp;
            <?php 
            if($_SESSION['auth_admin']['firstname']==null || $_SESSION['auth_admin']['firstname']="")
            {
              echo $_SESSION['auth_admin']['email'];
            }
            else
            {
              echo $_SESSION['auth_admin']['firstname'];
            } 
            ?>
          </a>
          <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
            <a class="dropdown-item" href="dbinteract.php?logout=true"><i class="fa fa-sign-out"></i>&nbsp;Log Out</a>
          </div>
        </li>
      </ul>
    </div>
  </nav>
    <!-- LARGE SCREEN Side NAV END -->
    <div class="row mr-0">
      <div class="col-2 d-none d-md-block">
        <div class="nav flex-column nav-pills left-navigation" id="v-pills-tab" role="tablist" aria-orientation="vertical">
          <a href="trainings.php" class="nav-link">Trainings</a>
          <a href="customers.php" class="nav-link">Customers</a>
          <a href="bookings.php" class="nav-link">Bookings</a>
          <a href="contact-details.php" class="nav-link">Contact Information</a>          
        </div>
      </div>
      <div class="col-12 col-sm-10 admin_main--content">