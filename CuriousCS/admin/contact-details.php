<?php
	session_start();
	if(!$_SESSION['auth_admin'])
	{
		header('location:login.php');
	}

include('admin-navbar.php');
require('../connection.php');
?>
<div class="container-fluid">
	<?php include('../messages.php'); ?>
	<div class="card mt-2">
		<div class="card-header">
			<span class="table-heading">Contact Information:</span>
		</div>
		<div class="card-body">
			<div class="table-responsive">	
				<table class="table">
					<thead>
						<tr>
							<th>S.N.</th>
							<th>Name</th>
							<th>Email</th>
							<th>Subject</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						<?php
							$q = "SELECT * from tbl_contacts";
							$data = $con->query($q);
							$n =1; //for serial number
							while($eachContact = $data->fetch_array())
							{
						?>
						<tr>
							<th scope="row"><?php echo $n; ?></th>
							<td><?php echo $eachContact['name'] ?></td>
							<td><?php echo $eachContact['email']; ?></td>
							<td><?php echo $eachContact['subject']; ?></td>
							<td class="text-right">
								<a type="button" class="btn btn-success btn-sm" href="contact.php?contactID=<?php echo $eachContact['id']?>"><i class="fa fa-search-plus" style="font-size: 15px;" ></i></a>
								<a type="button" class="btn btn-danger btn-sm" href="dbwork.php?deleteContact=<?php echo $eachContact['id'];?>" onClick="return confirm('Are you sure you want to delete contact details?')"><i class="fa fa-trash" style="font-size: 15px;"></i></a>
							</td>
						</tr>
						<?php
						$n = $n+1;
						}
						?>
					</tbody>
				</table>
			</div>
		</div>

</div>
<?php
include('admin-footer.php');
?>