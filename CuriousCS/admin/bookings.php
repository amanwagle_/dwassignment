<?php
	session_start();
	if(!$_SESSION['auth_admin'])
	{
		header('location:login.php');
	}
include('admin-navbar.php');
require('../connection.php');
?>
<div class="container-fluid">
	<?php include('../messages.php'); ?>
	<div class="card mt-2">
		<div class="card-header">
			<span class="table-heading">Bookings</span>
		</div>
		<div class="card-body">
			<div class="table-responsive">	
				<table class="table">
						<thead>
							<tr>
								<th>S.N.</th>
								<th>Name</th>
								<th>Email</th>
								<th>Booked Training</th>
								<th>Training Date</th>
								<th>Cost</th>
								<th>Booked On</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							<?php
								$q = "SELECT * from tbl_bookings";
								$data = $con->query($q);
								$n =1; //for serial number
								while($eachBooking = $data->fetch_array())
								{
									// //booked user details
									$user_query= "SELECT * from tbl_users where id=".$eachBooking['user_id'];
									$user_details=$con->query($user_query)->fetch_array();

									// //booked training details
									$training_query= "SELECT * from tbl_trainings where id=".$eachBooking['training_id'];
									$training_details=$con->query($training_query)->fetch_array(); 

							?>
								<tr>
									<th scope="row"><?php echo $n; ?></th>
									<td><?php echo $user_details['firstname']." ".$user_details['surname']; ?></td>
									<td><?php echo $user_details['email'];?></td>
									<td><?php echo $training_details['title']; ?></td>
									<td><?php echo $training_details['date']; ?></td>
									<td>$ <?php echo $training_details['cost']; ?></td>
									<td><small><?php echo $eachBooking['booked_date'];?></small></td>
									<td class="text-right">
										<a type="button" class="btn btn-danger btn-sm" href="dbwork.php?deleteBooking=<?php echo $eachBooking['id'];?>"><i class="fa fa-window-close" style="font-size: 15px;"
										onClick="return confirm('Are you sure you want to cancel booking?')"></i></a>
									</td>
								</tr>
							<?php
							$n = $n+1;
							}
							?>
						</tbody>
				</table>
			</div>
		</div>
	</div>
	
</div>


<?php
include('admin-footer.php');
?>