<?php
	session_start();
	if(!$_SESSION['auth_admin'])
	{
		header('location:login.php');
	}
include('admin-navbar.php');
require('../connection.php');

	$trainingID = $_GET['id'];

	$q = "SELECT * from tbl_trainings where id=".$trainingID;
	$training = $con->query($q)->fetch_array();

?>
<div class="container-fluid">
	<?php include('../messages.php'); ?>
	<div class="my-4">
		<div class="card mt-2">
			<div class="card-header">
				<span class="table-heading">Edit Trainings</span>
			</div>
			<div class="card-body">
				<form method="post" action="dbwork.php" enctype="multipart/form-data">
					<input type="hidden" name="id" value="<?php echo $training['id']; ?>">
					<div class="form-group">
						<label for="txttitle"><b>Title</b></label>
						<input type="text" class="form-control" id="txttitle" value="<?php echo $training['title']; ?>" name="title" required>
					</div>
					<div class="form-group">
						<label for="txtcs"><b>Area of Cyber Security</b></label>
						<input type="text" class="form-control" id="txtcs" value="<?php echo $training['area_covered']; ?>" name="cs" required>
					</div>
					<div class="form-group row">
						<div class="col-sm-6">
							<label for="txtcost"><b>Cost</b></label>
							<input type="number" class="form-control" id="txtcost" value="<?php echo $training['cost'];?>" name="cost" required>
						</div>
						<div class="col-sm-6">
							<label for="date"><b>Date</b></label>
							<input type="date" class="form-control" id="date" name="date" value="<?php echo $training['date']; ?>"required>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-sm-6">
							<label for="start-time"><b>Starts In</b></label>
							<input type="time" class="form-control" id="start-time" name="stime" value="<?php echo $training['start_time']; ?>" required>
						</div>
						<div class="col-sm-6">
							<label for="end-time"><b>Ends In</b></label>
							<input type="time" class="form-control" id="end-time" name="etime" value="<?php echo $training['end_time']; ?>" required>
						</div>
					</div>
					<div class="form-group">
						<label for="txtdes"><b>Description</b></label>
						<textarea class="form-control" id="txtdes" rows="4" name="des"><?php echo $training['des']; ?></textarea>
					</div>
					<button class="btn btn-success" type="submit" name="update-training">Save</button>
				</form>
			</div>
		</div>
	</div>
</div>

<?php
include('admin-footer.php');
?>