<?php
	session_start();
	if(!$_SESSION['auth_admin'])
	{
		header('location:login.php');
	}
include('admin-navbar.php');
require('../connection.php');
?>
<div class="container-fluid">

	<?php 
		include('../messages.php');
		$q = "SELECT * from tbl_users where `id`=".$_GET['customerID'];

		$customer_detail = $con->query($q)->fetch_array();
	?>

	<div class="card mt-2">
		<div class="card-header">
			<h5>Customer Details</h5>
		</div>
		<div class="card-body">
			<div class="container">
				<p><b>Name:</b> <?php echo $customer_detail['firstname']?></p>
				<p><b>Email:</b> <?php echo $customer_detail['email'];?></p>
				<p><b>Business Name:</b> <?php echo $customer_detail['business'];?></p>
				<p><b>Job Title:</b> <?php echo $customer_detail['job'];?></p>
				<p><b>Area of cyber security interest:</b><br> <?php echo $customer_detail['interest'];?></p>
			</div>
		</div>
		<div class="card-footer">
			<a type="button" class="btn btn-danger btn-sm" href="dbwork.php?deleteCustomer=<?php echo $customer_detail['id']; ?>" onClick="return confirm('Are you sure you want to delete Customer?')"><i class="fa fa-trash" style="font-size: 15px;">&nbsp; Delete</i></a>
		</div>
	</div>
</div>
<?php
include('admin-footer.php');
?>