<?php 
  session_start();
 ?>
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="../assets/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="../assets/css/admin.css">
		<title>Admin Panal</title>
  </head>
  <body class="text-center login-body">
    <div class="container-fluid">
    <?php include('../messages.php'); ?>
  	<div class="container">
    <form class="form-signin" method="post" action="dbwork.php">
      <img class="mb-4" src="../assets/images/logo.png" alt="logo" width="100" height="100">
      <h1 class="h3 mb-3 font-weight-normal">Please sign in</h1>
      <label for="inputEmail" class="sr-only">Email</label>
      <input type="text" class="form-control" placeholder="Email" name="email" required autofocus>
      <label for="inputPassword" class="sr-only">Password</label>
      <input type="password" id="inputPassword" class="form-control" placeholder="Password" name="password" required>
      <button class="btn btn-lg btn-primary btn-block" type="submit" name="login">Sign in</button>
      <p class="mt-5 mb-3 text-muted">&copy;Aman Wagle(2019-2020)</p>
    </form>
    </div>
    </div>
  </body>
</html>