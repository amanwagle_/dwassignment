<?php 

	if(isset($_SESSION['error']))
	{
		$error_message = $_SESSION['error'];
		unset($_SESSION['error']);
	}

	if(isset($_SESSION['success']))
	{
		$success_message = $_SESSION['success'];
		unset($_SESSION['success']);
	}

	if(isset($_SESSION['warning']))
	{
		$warning_message = $_SESSION['warning'];
		unset($_SESSION['warning']);
	}
?>

	<?php if(isset($success_message)){ ?>
		<div class="alert alert-success mt-2">
			<?php echo $success_message ?>
		</div>

	<?php } ?>

	<?php if(isset($error_message)){ ?>
		<div class="alert alert-danger mt-2">
			<?php echo $error_message ?>
		</div>
	<?php } ?>

	<?php if(isset($warning_message)){ ?>
		<div class="alert alert-warning mt-2">
			<?php echo $warning_message ?>
		</div>
	<?php } ?>



