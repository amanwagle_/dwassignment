<?php
	session_start();
	$title ="Contact Us - Curious cybersecurity";
	include("navbar.php");
?>
<div class="container-fluid">
	<?php include("messages.php") ?>
	<div class="container">
		<div class="row my-4">
			<div class="col-sm-6 contact-us mb-2">
				<form method="post" action="dbinteract.php">
					<h4>Reach Out To Us!</h4>
					<hr>
					<div class="form-group">
						<label for="txtname"><b>Name</b></label>
						<input type="text" class="form-control" id="txtname" placeholder="Full Name" name="name" required>
					</div>
					<div class="form-group">
						<label for="email"><b>Email address</b></label>
						<input type="email" class="form-control" id="email" placeholder="name@example.com" name="email" required>
					</div>
					<div class="form-group">
						<label for="num"><b>Phone Number</b></label>
						<input type="text" class="form-control" id="num" placeholder="Phone Number" name="num">
					</div>
					<div class="form-group">
						<label for="txtsubject"><b>Subject</b></label>
						<input type="text" class="form-control" id="txtsubject" placeholder="Subject" name="subject">
					</div>
					<div class="form-group">
						<label for="txtmessage"><b>Message</b></label>
						<textarea class="form-control" id="txtmessage" rows="5" placeholder="Message" name="message"></textarea>
					</div>
					<button class="btn btn-success" type="submit" name="contactus">Submit</button>
					<button class="btn btn-danger" type="reset">Reset</button>
				</form>
				
			</div>
			<div class="col-sm-6">
				<h4>Other Ways To Connect</h4>
				<hr>
				<div class="clearfix">
					<div class="pull-left contact-icon">
						<i class="fa fa-phone" aria-hidden="true"></i>
					</div>
					<div class="pull-left">
						<h5>Mobile Number</h5>
						<p>+977-9824127065</p>
						
					</div>
				</div>
				<div class="clearfix">
					<div class="pull-left contact-icon">
						<i class="fa fa-envelope" aria-hidden="true"></i>
					</div>
					<div class="pull-left">
						<h5>Email Address</h5>
						<p>info@curiouscs.com</p>
					</div>
				</div>
				<div class="contact-social-block">
					<h4>We Are Social!</h4>
					<hr>
					<div class="container">
						<ul>
							<li><a href="https://www.facebook.com/viralmeme2k19/" target="_blank"><i class="fa fa-facebook"></i></a></li>
							<li><a href="https://twitter.com/viralmeme2k19" target="_blank"><i class="fa fa-twitter"></i></a></li>
							<li><a href="https://www.instagram.com/viralmeme2k19/" target="_blank"><i class="fa fa-instagram"></i></a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php include("footer.php"); ?>