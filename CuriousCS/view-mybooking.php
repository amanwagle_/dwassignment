<?php
	session_start();
	if(!$_SESSION['auth_user'])
	{
		header('location:customerLogin.php');
	}
	$title ="View my booking - Curious cybersecurity";
	include("navbar.php");
	require('connection.php');
?>
	<div class="container-fluid">
		<?php require('messages.php');?>
		<div class="container">
			<?php
				$q = "SELECT * from tbl_bookings where user_id=".$_SESSION['auth_user']['id'];
				$data = $con->query($q);

				if($data->num_rows <= 0)
				{
				?>
					<div class="container text-center my-5" style="min-height: 250px;">
						<h3>You have no booking till now...</h3>	
						<a class="btn btn-success btn-lg" href="location.php">Book Now</a>
					</div>		
				<?php
				}
				else
				{
				while($eachBooking = $data->fetch_array())
				{
					// //booked training details
					$training_query= "SELECT * from tbl_trainings where id=".$eachBooking['training_id'];
					$training_details=$con->query($training_query)->fetch_array(); 
			?>
				<div class="card my-3">
					<div class="card-header">
						<h5><?php echo $training_details['title']; ?></h5>
					</div>
					<div class="card-content">
						<div class="row">
							<div class="col-sm-4">
								<img src="assets/images/<?php echo $training_details['image'];?>" alt="Training image" width="100%">				
							</div>
							<div class="col-sm-4 my-2">
								<p><b>Title:</b> <?php echo $training_details['title']; ?></p>
								<p><b>Date:</b> <?php echo $training_details['date']; ?></p>
								<p><b>Cost:</b> $ <?php echo $training_details['cost']; ?></p>					
							</div>
							<div class="col-sm-4 my-2">
								<p><b>Time:</b> <?php echo $training_details['start_time']." to ".$training_details['end_time']; ?></p>
								<p><b>Booked By:</b> <?php echo $_SESSION['auth_user']['email'];?></p>
								<p><b>Booked On:</b> <?php echo $eachBooking['booked_date'];?></p>			
							</div>					
						</div>
					</div>
					<div class="card-footer">
						<a href="edit-mybooking.php?id=<?php echo $eachBooking['id'];?>" class="btn btn-primary"><i class="fa fa-edit"></i>&nbsp;Edit Booking</a>
						<a href="dbinteract.php?deleteBooking=<?php echo $eachBooking['id'];?>" class="btn btn-danger" onClick="return confirm('Are you sure you want to cancel booking?')"><i class="fa fa-trash"></i>&nbsp;Cancel Booking</a>
					</div>
				</div>
			<?php 
				}
			}
			?>
		</div>	
	</div>
<?php
	include('footer.php');
?>