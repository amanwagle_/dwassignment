<?php
	session_start();
	$title ="Location - Curious cybersecurity";
	include("navbar.php");
	require('connection.php');

	$q = "select * from tbl_trainings";
	$data = $con->query($q);

	$trainings__ = [];
	while ($row = $data->fetch_array()) 
	{
	  foreach ($row as $key => $value) 
	  {
	    $trainings__[$row['id']][$key] = $value;
	  }
	}
 
?>
<div class="container-fluid">
	<?php
		require('messages.php');
		if(!isset($_SESSION['auth_user']))
		{
	?>
	<div class="container text-center my-5">
		<h5>If you like our services and wanna join our training system you can book different training sessions.</h5>
		<a class="btn btn-success btn-lg" href="customerLogin.php">Please login to Book trainings</a>
	</div>
	<?php
		}
		else
		{
	?>
	<div class="container card my-3">
		<div class="card-header">
			<h5>Book Training</h5>
		</div>
		<div class="card-body">
			<form method="post" action="dbinteract.php">
				<div class="form-group">
					<label for="txtemail"><b>Email</b></label>
					<input type="email" class="form-control" id="txtemail" name="email" value="<?php echo $_SESSION['auth_user']['email'] ?>" readonly>
				</div>
				<div class="form-group">
					<label><b>Training</b> <small>(Select training which you want to book.)</small></label>
					<select class="form-control trainings__select-control" name="book-trainingID" required>
						<option value="">--Select Training--</option>
					<?php
						foreach($trainings__ as $eachTraining)
						{
						 
					?>
						<option value="<?php echo $eachTraining['id']; ?>"><?php echo $eachTraining['title']; ?></option> 
					<?php 
						}
					 ?>
					</select>
				</div>
				<div class="form-group d-none training-date-field">
					<label>Date</label>
					<input type="text" name="trainingDate" class="form-control" readonly>
				</div>
				<div class="form-group d-none training-cost-field">
					<label>Cost</label>
					<input type="text" name="cost" class="form-control" readonly>
				</div>			
		</div>
		<div class="card-footer">
			<button type="submit" name="book" class="btn btn-primary" onClick="return confirm('Are you sure you want to proceed booking?')">Book</button>
			</form>
		</div>
	</div>
	<?php
		}
	?>
	
</div>
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2482.948134764036!2d-0.12696118447983315!3d51.514167518029694!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x487604cc5e90cfb5%3A0xbd636752f8fcc837!2sCurious!5e0!3m2!1sen!2snp!4v1556209535501!5m2!1sen!2snp" width="100%" height="550" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>

<script type="text/javascript">
	$(document).on('change', '.trainings__select-control', function(){
		var selected = $(this).val();

		if(selected != "" && selected != null)
		{
 			var trns = <?php  echo json_encode($trainings__) ?>;
 			$('.training-date-field').removeClass('d-none');
 			$('.training-date-field').find('input').val(trns[selected].date);
 			$('.training-cost-field').removeClass('d-none');
 			$('.training-cost-field').find('input').val(trns[selected].cost);
		}
		else
		{
 			$('.training-date-field').find('input').val("");
 			$('.training-date-field').addClass('d-none');
 			$('.training-cost-field').find('input').val("");
 			$('.training-cost-field').addClass('d-none');
		}
	})
</script>
<?php
include('footer.php');
?>