<?php 
	session_start();
	$title ="Our approach - Curious cybersecurity";
	include("navbar.php"); 
	require('connection.php');
?>
<div class="container-fluid">
	<?php include('messages.php') ?>
	<div class="container mt-4">
		<div class="row">
			<?php
				$q = "select * from tbl_trainings";
				$data = $con->query($q);
				while($eachTraining = $data->fetch_array())
				{
			?>
			<div class="col-md-4 mb-5 single--training">
				<div class="card">
					<div style="height:200px; overflow: hidden;">
						<img class="card-img-top" src="assets/images/<?php echo $eachTraining['image'];?>" alt="Card image cap" >
					</div>
					<div class="card-body">
						<h5><?php echo $eachTraining['title']; ?></h5>
						<hr>
						<p><b>Event Date : </b><?php echo $eachTraining['date'] ?></p>
						<p><b>Area Covered : </b><?php echo $eachTraining['area_covered'] ?></p>
						<p><?php echo substr($eachTraining['des'], 0, 150).'...'; ?></p>
						<a href="view-trainings.php?trainingID=<?php echo $eachTraining['id'];?>" class="btn btn-primary">View</a>
					</div>
				</div>
			</div>
			<?php
			}
			?>
			
		</div>
	</div>
</div>
<?php include("footer.php"); ?>