$(document).on('click', '.btn--cookie_accept', function(e){
	e.preventDefault();
  	
  	$.ajax({
  		url:'processCookie.php',
  		data:{setCookie:true},
  		type:'post',
      dataType:'JSON',
  		success:function(response){
  			if(response.success == true){
          $('.website--cookie_section').fadeOut();
        }
  		}
  	})
})