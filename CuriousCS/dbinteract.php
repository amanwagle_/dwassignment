<?php 
	session_start();

	require("connection.php");
	
	if(isset($_POST['register']))
	{
		$email = $_POST['email'];
	 	$password = $_POST['password'];

	 	if(isset($_POST['agree']))
	 	{
		 	$select = "SELECT * from `tbl_users` where `email`='".$email."'";

		 	$data = $con->query($select)->fetch_array();

		 	if($data)
		 	{
		 		$_SESSION['error']='Email already exits.';
		 		header('location:index.php');
		 	}
		 	else
		 	{
		 		$customer_number = rand(100000,999999)."_CUR";
		 		$q = "INSERT INTO `tbl_users`(`id`,`email`,`password`,`customer_number`) VALUES (null, '$email','$password','$customer_number')";
			 	$con -> query($q);
			 	$_SESSION['success']='You have been registered successfully.';
			 	header('location:index.php');
		 	}
	 	}
	 	else
	 	{
	 		$_SESSION['warning']='Please agree terms and conditions.';
		 	header('location:index.php');
	 	}
	}


	if(isset($_POST['edit']))
	{
		$fname = $_POST['fname'];
		$sname = $_POST['sname'];
		$email = $_POST['email'];
		$business = $_POST['business'];
		$job = $_POST['job'];
		$interest = $_POST['interest'];

		$q = "UPDATE `tbl_users` set `firstname` = '$fname', `surname` = '$sname', `email` = '$email', `business` = '$business', `job` ='$job', `interest` = '$interest' where `id`=".$_SESSION['auth_user']['id'];
		$con -> query($q);

		// replacing present session with updated password sesssion
		$query = "SELECT * from `tbl_users` where `id` =".$_SESSION['auth_user']['id'];
		$user = $con->query($query)->fetch_array();
		$_SESSION['auth_user'] = $user;
		$_SESSION['success']='User details updated successfully';

		header('location:profile.php');
	}	


	if(isset($_POST['changepw']))
	{
		$currentpw = $_POST['cupassword'];
		$newpw = $_POST['npassword'];
		$confirmpw = $_POST['copassword'];

		
		if($_SESSION['auth_user']['password']==$currentpw)
		{
			
			if($newpw == $confirmpw)
			{
				$q = "UPDATE `tbl_users` set `password`='$newpw' where `id` =".$_SESSION['auth_user']['id'];
				$con -> query($q);

				// replacing present session with updated password sesssion
				$query = "SELECT * from `tbl_users` where `id` =".$_SESSION['auth_user']['id'];
				$user = $con->query($query)->fetch_array();
				$_SESSION['auth_user'] = $user;
				$_SESSION['success']='Password successfully changed';
				header('location:profile.php');
			}
			else
			{
				$_SESSION['error'] = 'Password confirmation does not match!!!';
				header('location:profile.php');
			}
		}
		else
		{		 
			$_SESSION['error'] = 'Current Password does not match!!!';
			header('location:profile.php');
		}
	}


	if(isset($_POST['login']))
	{
		//checking cookie to block login attamp more than three times
		if(isset($_COOKIE['error_count']))
		{
			if($_COOKIE['error_count']>=2)
			{
				$_SESSION['error'] = 'You are locked out for 3 minutes due to too many attemtps. Please try again after  3 minutes.';
				header('location:customerLogin.php');
				die();
			}
		}
		$email = $_POST['email'];
		$pw = $_POST['password'];
		$query = "SELECT * from `tbl_users` where email='$email' and password='$pw'";
		$user = $con->query($query)->fetch_array();
		if($user)
		{
			$_SESSION['auth_user'] = $user;
			setcookie('error_count', '', time()-180);
			header('location:index.php');
		}
		else
		{
			//seting cookie to count login attamps
			setcookie('error_count',1,time()+180);
			if(isset($_COOKIE['error_count']))
			{
				setcookie("error_count", $_COOKIE['error_count']+1,time()+180);
				$_SESSION['error'] = "Password or Email Invalid!!! <b>".(2-$_COOKIE['error_count'])." attempts left</b>";
				header('location:customerLogin.php');
			}
			else
			{
				$_SESSION['error'] = 'Password or Email Invalid!!! <b> 2 attempts left</b>';
				header('location:customerLogin.php');
			}
		}	 
	}



	if(isset($_POST['contactus']))
	{
		$name = $_POST['name'];
	 	$email = $_POST['email'];
	 	$pnumber = $_POST['num'];
	 	$sub = $_POST['subject'];
	 	$msg = $_POST['message'];

	 	$q = "INSERT INTO `tbl_contacts`(`id`,`name`,`email`,`phone_num`,`subject`,`message`) VALUES (null,'$name','$email', '$pnumber','$sub','$msg')";

	 	$con -> query($q);
	 	$_SESSION['success']='Thank you for contacting us. We will get back to you ASAP.';
	 	header('location:contactus.php');
	}


	if(isset($_POST['book']))
	{
		if(!$_SESSION['auth_user'])
		{
			header('location:customerLogin.php');
			die();
		}

		$trainingId = $_POST['book-trainingID'];
		$userId = $_SESSION['auth_user']['id'];
		$date = date('Y-m-d H:i:s');

		$q = "SELECT * from `tbl_bookings` where `user_id`=".$userId." and `training_id`=".$trainingId;
		$booking = $con->query($q)->fetch_array();
		

		if($booking)
		{
			$_SESSION['warning']='You have already booked this training.';
	 		header('location:location.php');
	 	}
	 	else
	 	{
		$trainingDate = $_POST['trainingDate'];
		$today = date("Y-m-d");

			if ($trainingDate < $today) 
			{
				$_SESSION['warning']='You cannot book training in the day of training or training already occured.';
	 			header('location:location.php');
			}
			else
			{
		 		$q = "INSERT into `tbl_bookings`(`id`,`user_id`,`training_id`,`booked_date`) VALUES(null,'$userId','$trainingId','$date')";
				$con->query($q);
				$_SESSION['success']='Training booked successfully.';
	 			header('location:view-mybooking.php');
			}
		}
	}


	//logout
	if(isset($_GET['logout']))
	{
		unset($_SESSION['auth_user']);
		header('location:index.php');
	}


	//delete or cancel booking
	if(isset($_GET['deleteBooking']))
	{
		$bookingID = $_GET['deleteBooking'];
		$q = "DELETE FROM `tbl_bookings` WHERE `id`=".$bookingID." and `user_id`=".$_SESSION['auth_user']['id'];
		$con->query($q);

		$_SESSION['success']='Booking cancelled successfully.';
		header('location:view-mybooking.php'); 
	}




	if(isset($_POST['editBooking']))
	{
		if(!$_SESSION['auth_user'])
		{
			header('location:customerLogin.php');
			die();
		}

		$bookingId = $_POST['id'];
		$trainingId = $_POST['book-trainingID'];
		$date = date('Y-m-d H:i:s');

		$q = "SELECT * from `tbl_bookings` where `user_id`=".$_SESSION['auth_user']['id']." and `training_id`=".$trainingId;
		$booking = $con->query($q)->fetch_array();
		

		if($booking)
		{
			$_SESSION['warning']='You have already booked this training. Check once in view my booking page.';
	 		header('location:view-mybooking.php');
	 	}
	 	else
	 	{
		$trainingDate = $_POST['trainingDate'];
		$today = date("Y-m-d");

			if ($trainingDate < $today) 
			{
				$_SESSION['warning']='You cannot update booking in the day of training or training already occured.';
	 			header('location:view-mybooking.php');
			}
			else
			{
		 		$q = "UPDATE into `tbl_bookings` SET `training_id`='$trainingId',`booked_date`='date' where `id`=".$bookingId;
				$con->query($q);
				$_SESSION['success']='Booking Updated Successfully.';
	 			header('location:view-mybooking.php');
			}
		}
	}
 ?>

 