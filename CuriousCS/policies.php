<?php 
session_start();
	$title ="Policies - Curious cybersecurity";
	include("navbar.php"); 
?>


	<div class="container">
		<h5>Download research papaers</h5>
		<ul>
			<li><a href="CyberSecurity_2015.pdf" download>Cyber Security 2015</a></li>
			<li><a href="CyberSecurity_2016.pdf" download>Cyber Security 2016</a></li>
			<li><a href="CyberSecurity_2017.pdf" download>Cyber Security 2017</a></li>
			<li><a href="CyberSecurity_2018.pdf" download>Cyber Security 2018</a></li>
			<li><a href="CyberSecurity_2019.pdf" download>Cyber Security 2019</a></li>
			<li><a href="LB_cybersecurity_WEB.pdf" download>LB cybersecurity WEB</a></li>
			<li><a href="WorldofCyberSecurityandCyberCrime.pdf" download="">World of CyberSecurity and CyberCrime</a></li>
		</ul>
		<h5 class="mt-2">Privacy Policies</h5>
		<p>You may consult this list to find the Privacy Policy for each of the advertising partners of Website Name.
		Third-party ad servers or ad networks uses technologies like cookies, JavaScript, or Web Beacons that are used in their respective advertisements and links that appear on Website Name, which are sent directly to users' browser. They automatically receive your IP address when this occurs. These technologies are used to measure the effectiveness of their advertising campaigns and/or to personalize the advertising content that you see on websites that you visit.

		Note that Website Name has no access to or control over these cookies that are used by third-party advertisers.</p>

	</div>

<?php include("footer.php"); ?>
