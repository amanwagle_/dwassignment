<!DOCTYPE html>
<html>
  <head>
    <title><?php echo $title; ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="assets/css/font-awesome.css">
    <link rel="stylesheet" type="text/css" href="assets/css/style.css">
    <script type="text/javascript" src="assets/js/jquery.min.js"></script>

  </head>
  <body>

  <div class="container-fluid header d-none d-md-block">
    <div class="logo--image">
      <a href="index.html"><img src="assets/images/logo.png" class="mt-2"></a>
    </div>
    <p> <span>Curious </span>CyberSecurity</p>

    <h5 class="float-right mt-5">Secure digial world is our main objective.</h5>
  </div>
  <nav class="navbar navbar-expand-lg navbar-dark" style="background-color: #1570A5;">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
    </button>
    <img src="assets/images/logo.png" class="d-sm-none" width="50px">
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav mr-auto">
        <li class="nav-item active">
          <a class="nav-link" href="index.php">Home <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="services.php">Services</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="contactus.php">Contact Us</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="our-approach.php">Our Approach</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="location.php">Location</a>
        </li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <?php 
          if(!isset($_SESSION['auth_user']))
          {
         ?>
         <li class="nav-item">
          <a class="nav-link" href="customerLogin.php"><i class="fa fa-user"></i>&nbsp;Log In</a>
        </li>

        <?php 
          }
          else
          {
         ?>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-user"></i>&nbsp;
            <?php 
            if($_SESSION['auth_user']['firstname']==null || $_SESSION['auth_user']['firstname']="")
            {
              echo $_SESSION['auth_user']['email'];
            }
            else
            {
              echo $_SESSION['auth_user']['firstname'];
            } 
            ?>
          </a>
          <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
            <a class="dropdown-item" href="profile.php"><i class="fa fa-user"></i>&nbsp;Profile</a>
            <a class="dropdown-item" href="view-mybooking.php"><i class="fa fa-eye"></i>&nbsp;View My Bookings</a>
            <a class="dropdown-item" href="dbinteract.php?logout=true"><i class="fa fa-sign-out"></i>&nbsp;Log Out</a>
          </div>
          <?php 
            }
           ?>

        </li>
      </ul>
    </div>
  </nav>