<!-- Footer -->
<footer class="page-footer font-small text-white" style="background-color: #000;">
  
  <div class="container text-center text-md-left">
    
    <div class="row footer-link">
      <div class="col-sm-4 mt-4">
        <h5>ABOUT US</h5>
        <ul class="nav flex-column">
          <li class="nav-item"><a class="nav-link" href="company.php">Company</a></li>
          <li class="nav-item"><a class="nav-link" href="product.php">Product</a></li>
          <li class="nav-item"><a class="nav-link" href="partners.php">Partners</a></li>
          <li class="nav-item"><a class="nav-link" href="policies.php">Policies</a></li>
        </ul>
      </div>
      <div class="col-sm-4 mt-4">
        <h5>HELPFUL LINKS</h5>
        <ul class="nav flex-column">
          <li class="nav-item"><a class="nav-link" target="_blank" href="https://online.maryville.edu/blog/digital-forensics/">Maryville University</a></li>
          <li class="nav-item"><a class="nav-link" target="_blank" href="https://www.ncsc.gov.uk/">NCSC</a></li>
          <li class="nav-item"><a class="nav-link" target="_blank" href="https://www.cybersecurity.ox.ac.uk/">University of Oxford</a></li>
          <li class="nav-item"><a class="nav-link" target="_blank" href="https://nationalcrimeagency.gov.uk/">National Crime Agency</a></li>
        </ul>      
      </div>
      <div class="col-sm-4 mt-4">
          <form>
            <div class="input-group">
              <input class="form-control" type="search" placeholder="Your Keyword Here...">
                <div class="input-group-prepend">
                  <button class="btn btn-outline-success" type="submit"><i class="fa fa-search"></i></button>
                </div>
            </div>
          </form>

          <div class="footer-social-block mb-2">
          <h4>We Are Social!</h4>
          <div class="container">
            <ul>
              <li><a href="https://www.facebook.com/viralmeme2k19/" target="_blank"><i class="fa fa-facebook"></i></a></li>
              <li><a href="https://twitter.com/viralmeme2k19" target="_blank"><i class="fa fa-twitter"></i></a></li>
              <li><a href="https://www.instagram.com/viralmeme2k19/" target="_blank"><i class="fa fa-instagram"></i></a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    
  </div>
  <div class="footer-copyright text-center py-3">
    <p>© 2019 CuriousCS. All rights reserved.</p>
  </div>
  
</footer>

<?php 
    if(!isset($_COOKIE['cookies']))
      {
     ?>
<div class="container-fluid cookies website--cookie_section">
  <div class="col-md-12">

     <div>
       This website uses cookies to ensure you get the best experience on our website.&nbsp;&nbsp;
       <button name="Gotit" class="btn btn-sm btn-warning btn--cookie_accept">Got It</button>
     </div>

  </div>
</div>
<?php 
  }

?>

<script type="text/javascript" src="assets/js/bootstrap.js"></script>
<script type="text/javascript" src="assets/js/costum.js"></script>
</body>
</html>