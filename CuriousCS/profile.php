<?php
	session_start();

	if(!$_SESSION['auth_user'])
	{
		header('location:customerLogin.php');
	}
	$title ="Profile - Curious cybersecurity";
	require('connection.php');
	include("navbar.php"); 
?>

<div class="container-fluid">
	
	<?php require('messages.php'); ?>

	<div class="row justify-content-center">
		<div class="col-md-6 my-4 profile">
			<hr>
			<h2 class="mt-3">Profile</h2>
			<hr>
			<div class="image-container mx-auto my-4">
				<img src="assets/images/hacker.png" alt="Avatar" class="img-thumbnail image">
				<div class="container image-overlay">
					<div class="row text-center" style="margin-top: 25px;">
						<div class="col-md-6"><a href="" class="btn btn-info px-3"><i class="fa fa-search"></i></a></div>
						<div class="col-md-6"><a href="" class="btn btn-info px-3"><i class="fa fa-edit"></i></a></div>
					</div>
				</div>
			</div>
			<div class="container-fluid text-center">
				<p><b>Name:</b> <?php echo $_SESSION['auth_user']['firstname']." ".$_SESSION['auth_user']['surname'];?></p>
				<p><b>Email:</b> <?php echo $_SESSION['auth_user']['email'];?></p>
				<p><b>Business Name:</b> <?php echo $_SESSION['auth_user']['business']; ?></p>
				<p><b>Job Title:</b> <?php echo $_SESSION['auth_user']['job']; ?></p>
				<p><b>Area of cyber security interest:</b> <?php echo $_SESSION['auth_user']['interest']; ?></p>
			</div>
			<div class="container col-auto float-right my-3">
				<a href="" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#UpdateDetail">Edit Details</a>
				<a href="" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#ChangePassword">Change Password</a>
			</div>

			<!-- PopUp for  edit profile-->
			<div class="modal fade" id="UpdateDetail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
				<div class="modal-dialog modal-dialog-centered">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLongTitle">Update Details</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
							</button>
						</div>

						<form method="post" action="dbinteract.php" enctype="multipart/form-data">
							<div class="modal-body">
								<div class="form-group">
									<label for="fname">First Name</label>
									<input type="text" class="form-control" id="fname" placeholder="firstname" value="<?php echo $_SESSION['auth_user']['firstname']; ?>" name="fname">
								</div>
								<div class="form-group">
									<label for="sname">Surname</label>
									<input type="text" class="form-control" id="sname" placeholder="Surname" value="<?php echo $_SESSION['auth_user']['surname']; ?>" name="sname">
								</div>
								<div class="form-group">
									<label for="email">Email</label>
									<input type="Email" class="form-control" id="email" placeholder="Email" value="<?php echo $_SESSION['auth_user']['email']; ?>" name="email" readonly>
								</div>
								<div class="form-group">
									<label for="bus">Business Name</label>
									<input type="text" class="form-control" id="bus" placeholder="Business Name" value="<?php echo $_SESSION['auth_user']['business']; ?>" name="business">
								</div>
								<div class="form-group">
									<label for="job">Job Title</label>
									<input type="text" class="form-control" id="job" placeholder="Job Title" value="<?php echo $_SESSION['auth_user']['job']; ?>" name="job">
								</div>
								<div class="form-group">
									<label for="interest">Area of Interest</label>
									<input type="text" class="form-control" id="interest" placeholder="Area of Interest" value="<?php echo $_SESSION['auth_user']['interest']; ?>" name="interest">
								</div>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
								<button type="submit" class="btn btn-primary" name="edit">Save changes</button>
							</div>
						</form>
					</div>
				</div>
			</div>

			
			<!-- PopUp for change pw-->
			<div class="modal fade" id="ChangePassword" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
				<div class="modal-dialog modal-dialog-centered">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLongTitle">Change Password</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
							</button>
						</div>
						
						<form method="post" action="dbinteract.php" enctype="multipart/form-data">
							<div class="modal-body">
								<div class="form-group">
									<label for="old_password">Current Password</label>
									<input type="password" class="form-control " id="old_password" placeholder="Current Password" name="cupassword" required>
									 
								</div>
								<div class="form-group">
									<label for="new_password">New Password</label>
									<input type="password" class="form-control " id="new_password" placeholder="New Password" name="npassword" required>
								</div>
								<div class="form-group">
									<label for="confirm_password">Confirm Password</label>
									<input type="password" class="form-control " id="confirm_password" placeholder="Confirm Password" name="copassword" required>
								</div>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
								<button type="submit" class="btn btn-primary btn--password_change" name="changepw">Save changes</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php include("footer.php") ?>
