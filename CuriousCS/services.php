<?php
 $title ="Our Services - Curious cybersecurity";
 include("navbar.php"); ?>

	<div class="container my-4">		
		<h5>Video file about Cyber Security</h5>
		<iframe width="100%" height="400" src="https://www.youtube.com/embed/sdpxddDzXfE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
		<hr>
		<h5>Audio file about Cyber Security</h5>
		<audio controls style="width:100%;">
			<source src="assets/audios/cybersecurity.mp3" type="">
		</audio>
		<hr>
		<div class="card">
			<div class="card-header">
				<h3>Service Provided</h3>
			</div>
			<div class="card-body" style="font-size:20px;">
				<ul>
					<li>Security Program Strategy to align information security policy and strategy with business goals.</li>
					<li>Threat and Vulnerability Management to uncover and remediate flaws and vulnerabilities in your security systems.</li>
					<li>Enterprise Risk and Compliance to better understand risk through an IT risk assessment and make informed decisions about managing it.</li>
					<li>Security Architecture and Implementation to help you make decisions about the right technology, architecture and projects to ensure enterprise and network security</li>
					<li>Enterprise Incident Management to improve response to unauthorized intrusion attacks.</li>
					<li>Identity and Access Management (IAM) to design, implement and test IAM systems that better enable business.</li>
					<li>Education and Awareness to promote behavior that can improve security and reduce risk.</li>
					<li>Managed Security Services to provide your team with turnkey security solutions.</li>
				</ul>
			</div>
		</div>
	</div>

<?php include("footer.php"); ?>
