<?php 
	session_start();
	$title ="Training Details - Curious cybersecurity";
	include("navbar.php"); 
	require('connection.php');
?>
<div class="container-fluid">
	<?php 
		include('messages.php'); 

		$q = "select * from tbl_trainings where id=".$_GET['trainingID'];
		$trainingData=$con->query($q)->fetch_array();		
	?>

	<div class="container my-5">
		<h2><?php echo $trainingData['title'] ?></h2>
			<div class="row">
			<div class="col-md-6">
				<img src="assets/images/<?php echo $trainingData['image'];?>" alt="Training image" width="100%">
			</div>
			<div class="col-md-6">
				<p><b>Date: </b> <?php echo $trainingData['date'] ?></p>
				<p><b>Cost: </b>$<?php echo $trainingData['cost'] ?></p>
				<p><b>Time: </b> <?php echo $trainingData['start_time']?> - <?php echo $trainingData['end_time'] ?></p>
				<p><b>Description: </b> <?php echo $trainingData['des'] ?></p>
				<a href="location.php" class="btn btn-primary">Book</a>
			</div>
		</div>
	</div>
	

</div>

<?php include("footer.php"); ?>
