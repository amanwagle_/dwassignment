<?php 
  session_start();
  $title ="Log In - Curious cybersecurity";
  require('navbar.php'); 
?>

    <div class="container-fluid">

    <?php include('messages.php'); ?>

      <div class="row justify-content-center">
        <div class="col-md-6 my-4 profile">
          <form class="form-signin" method="post" action="dbinteract.php">
            <hr>
            <h1 class="h3 mb-3 font-weight-normal">LogIn</h1>
            <hr>
            <div class="container mb-4">
            <div class="form-group">
              <label for="inputEmail">Email</label>
              <input type="text" class="form-control" placeholder="Email" id="inputEmail" name="email" required autofocus>
            </div>
            <div class="form-group">
              <label for="inputPassword">Password</label>
              <input type="password" id="inputPassword" class="form-control" placeholder="Password" name="password" required>
            </div>
            <button type="submit" href="" class="btn btn-primary" name="login">Login</button>
          </div>
          </form>
        </div>
      </div>
    </div>
<?php require('footer.php'); ?>
