<?php
	session_start();
	if(!$_SESSION['auth_user'])
	{
		header('location:customerLogin.php');
	}
	$title ="Edit Bookings - Curious cybersecurity";
	include("navbar.php");
	require('connection.php');


	$bookingId = $_GET['id'];

	$q = "select * from tbl_trainings";
	$data = $con->query($q);

	$trainings__ = [];
	while ($row = $data->fetch_array()) 
	{
	  foreach ($row as $key => $value) 
	  {
	    $trainings__[$row['id']][$key] = $value;
	  }
	} 

?>
<div class="container-fluid">
	<?php
		require('messages.php');
		if(!isset($_SESSION['auth_user']))
		{
	?>
	<div class="container text-center my-3">
		<a class="btn btn-success btn-lg" href="customerLogin.php">Please login to Book trainings</a>
	</div>
	<?php
		}
		else
		{
	?>
	<div class="container card my-3">
		<div class="card-header">
			<h5>Update Booked Training</h5>
		</div>
		<div class="card-body">
			<form method="post" action="dbinteract.php">
				<input type="hidden" name="id" value="<?php echo $bookindId;?>">
				<div class="form-group">
					<label for="txtemail"><b>Email</b></label>
					<input type="email" class="form-control" id="txtemail" name="email" value="<?php echo $_SESSION['auth_user']['email'] ?>" readonly>
				</div>
				<div class="form-group">
					<label><b>Training</b> <small>(Select training which you want to book.)</small></label>
					<select class="form-control trainings__select-control" name="book-trainingID" required>
					<?php

						$select = "SELECT * from `tbl_bookings` where id=".$bookingId;
						$booking_data = $con->query($select)->fetch_array();

						foreach($trainings__ as $eachTraining)
						{						 
					?>
						<option value="<?php echo $eachTraining['id']; ?>" <?php echo($eachTraining['id']=$booking_data['training_id'])?'selected="selected"':'';?>><?php echo $eachTraining['title'] ?></option> 
					<?php 
						}
					 ?>
					</select>
				</div>
				<div class="form-group training-date-field">
					<label>Date</label>
					<input type="text" name="trainingDate" class="form-control" readonly>
				</div>
				<div class="form-group training-cost-field">
					<label>Cost</label>
					<input type="text" name="cost" class="form-control" readonly>
				</div>			
		</div>
		<div class="card-footer">
			<button type="submit" name="editBooking" class="btn btn-primary" onClick="return confirm('Are you sure you want to update booking?')">Update Booking</button>
			</form>
		</div>
	</div>
	<?php
		}
	?>
	
</div>
<script type="text/javascript">
	$(document).on('change', '.trainings__select-control', function(){
		var selected = $(this).val();
 			var trns = <?php  echo json_encode($trainings__) ?>;

 			$('.training-date-field').find('input').val(trns[selected].date);
 			$('.training-cost-field').find('input').val(trns[selected].cost);

	})
	$('.trainings__select-control').trigger('change');
	
</script>
<?php
include('footer.php');
?>