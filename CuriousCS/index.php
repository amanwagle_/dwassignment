<?php
session_start();
$title ="Welcome - Curious cybersecurity";
	include("navbar.php");
?>
<div class="container-fluid mt-1" style="position: relative;">
	<?php include("messages.php") ?>
	<div class="carousel-overlay text-center d-none d-md-block">
    	<h2>Welcome to Curious CyberSecurity</h1>
    		<p>
    			We try our best to secure you because we care.
    		</p>
    	<a class="btn btn-success" style="color:white">Get started with our trainings</a>
    </div>
	<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
		<ol class="carousel-indicators">
			<li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
			<li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
			<li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
			<li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
		</ol>
		<div class="carousel-inner">
			<div class="carousel-item active">
				<img class="d-block w-100" src="assets/images/bannar1.jpg" alt="First slide">
			</div>
			<div class="carousel-item">
				<img class="d-block w-100" src="assets/images/bannar2.jpg" alt="Second slide">
			</div>
			<div class="carousel-item">
				<img class="d-block w-100" src="assets/images/bannar4.jpg" alt="Third slide">
			</div>
			<div class="carousel-item">
				<img class="d-block w-100" src="assets/images/bannar3.png" alt="Fourth slide">
			</div>
		</div>
		<a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
			<span class="carousel-control-prev-icon" aria-hidden="true"></span>
			<span class="sr-only">Previous</span>
		</a>
		<a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
			<span class="carousel-control-next-icon" aria-hidden="true"></span>
			<span class="sr-only">Next</span>
		</a>
	</div>

	
</div>
<div class="container mt-2 text-center">
	<h1>Curious CyberSecurity</h1>
	<p>Curious Cybersecurity are a research-based company founded in 2016, created by John and Steven Yung.  Their ambition is to research and help companies with the human elements of cybersecurity with a view to developing anti-phishing filtering solutions.
		<br>
		The company formed when the brothers left University and were subject to a number of phishing attacks which disabled their website for a number of days.  They produce press releases and provide training to companies with a view to strengthening cybersecurity defences.
	</p>
</div>
<div class="container mt-4 text-center">
	<h3>Wanna join us???</h3>
	<div>
		<a class="btn btn-success py-2 px-5" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">
			Get Started
		</a>
	</div>
	<div class="collapse mt-3" id="collapseExample">
		<div class="card card-body">
			<form method="post" action="dbinteract.php" enctype="multipart/form-data"	>
				<div class="form-group">
					<label for="Email1">Email address</label>
					<input type="email" class="form-control" id="Email1" placeholder="Enter email" name="email">
					<small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
				</div>
				
				<!-- Button trigger modal -->
				<button type="button" class="btn btn-primary float-right" data-toggle="modal" data-target="#exampleModal">
				Get Started
				</button>
				<!-- Modal -->
				<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title" id="exampleModalLabel">Register</h5>
							</div>
							<div class="modal-body">
								<div class="form-group">
									<label for="Password">Password</label>
									<input type="password" class="form-control" id="Password" placeholder="Password" name="password">
								</div>
								<div class="form-check">
									<input type="checkbox" class="form-check-input" id="Check" name="agree">
									<label class="form-check-label" for="Check">I agree to terms and conditions. <a href="policies.php">Learn more...</a></label>
								</div>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
								<button type="Submit" class="btn btn-success" name="register">Submit</button>
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
<div class="container-fluid mt-4 py-2" style="background-color: #ccc">
	<div class="container">
		<div class="row text-justify">
			<div class="col-md-4 py-3">
				<h3>News</h3>
				<img src="assets/images/hacker.png" class="img-thumbnail" width="100%" alt="News">
				<h5>Tech firms welcome govt data sharing</h5>
				<p>Trade association techUK has welcomed an announcement that the cyber security arm of GCHQ, the National Cyber Security Centre (NCSC), will increase its collaboration with the private and public sectors in order to share information about potential cyber-security attacks.
				“[GCHQ head] Jeremy Fleming’s speech today is an important acknowledgement that the NCSC can do more and scale its capability in sharing time-critical information with industry," said techUK's head of cyber Talal Rajab.</p>
			</div>
			<div class="col-md-4 py-3">
				<h3>Awards</h3>
				<img src="assets/images/award.jpg" class="img-thumbnail" width="100%" alt="Awards">
				<h5>SC awards</h5>
				<p>
					The mission of SC Awards is to honor the achievements of the cybersecurity brands and professionals striving to safeguard businesses, their customers, and critical data in North America. This exciting program of nominations, judging, and the announcement of finalists culminates in the SC Awards dinner, a gala evening convening hundreds of the industry’s brightest luminaries to network, toast the winners, and celebrate the best the cybersecurity community has to offer.
					We hope you'll join us in San Francisco on Tuesday, March 5 for the celebration!
				</p>
			</div>
			<div class="col-md-4 py-3">
				<h3>Paper Releases</h3>
				<img src="assets/images/news.jpg" class="img-thumbnail" width="100%" alt="Papers">
				<h5>Call for papers</h5>
				<p>On 9 and 10 October 2019, Cloud & Cyber Security Expo, Singapore will open its doors once again delivering top-quality delegates, with significant budgets, looking to source for the most cutting-edge and secure cloud & cyber security solutions.
				Cloud & Cyber Security Expo, Singapore 2019, co-located with award-winning technology event stack Cloud Expo Asia, Big Data World, Smart IoT Singapore, Data Centre World, eCommerce Expo Asia and new launch event - Technology for Marketing Asia is set to welcome 15,000 senior IT decision makers from end-users, enterprises.</p>
			</div>
		</div>
	</div>
</div>
<?php include("footer.php") ?>